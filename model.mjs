import { getForm, complete, tasksContainer, optionContainer, btnOptions, eraseButton, fragment } from "./main.mjs";
const generateID = () => Math.random().toString()
let tasks = []
const printHTML = (tasks) => {
    tasksContainer.textContent = ""
    tasks.forEach(taskObj => {
        const { id, task, finish } = taskObj
        const li = document.createElement("LI")
        li.classList.add("task")
        const div = document.createElement("DIV")
        div.classList.add("task__check")
        div.innerHTML = `
         <input class="task__input" type="checkbox" id="${id}">
         <label  class="task__label"  for="${id}">${task}.</label>
      `
        if (finish) div.firstElementChild.checked = true
        li.appendChild(div)

        const i = document.createElement("I")
        i.classList.add("fas", "fa-trash", "task__erase")
        if (complete) {
            eraseButton.classList.add("erase-button--active")
            i.classList.add("task__erase--active")
        } else {
            eraseButton.classList.remove("erase-button--active")
        }
        i.dataset.id = id
        li.appendChild(i)
        fragment.appendChild(li)
    })
    tasksContainer.appendChild(fragment)
}

const getAddTask = task => {

    const taskObj = {
        id: generateID(),
        task: task,
        finish: false,
    }
    tasks = [...tasks, taskObj]
    printHTML(tasks)
}

export { printHTML, getAddTask, generateID,tasks }