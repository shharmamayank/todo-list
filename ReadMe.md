# To-do Application Project

![image](./img/Screenshot%20from%202023-03-24%2015-08-38.png)


### To do this challenge, you need a basic understanding of HTML, CSS and JavaScript.

# Description

* Input area to give the work or add the checkbox
* It have three states: All, Active , Complete

### In All State

* Shows all the task
![image](./img/Screenshot%20from%202023-03-24%2015-09-03.png)

### In Active State 
* It all show the pending work 
![image](./img/Screenshot%20from%202023-03-24%2015-09-10.png)

### In Complete State 
* It show only the completed one which has been checked by All or active state
![image](./img/Screenshot%20from%202023-03-24%2015-09-15.png)



# Hosted Link -->
https://storied-choux-214e01.netlify.app/
