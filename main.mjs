import { printHTML, getAddTask, generateID, tasks } from "../model.mjs";
const getForm = document.getElementById("to-do-form")
const optionContainer = document.getElementById("to-do-options")
const tasksContainer = document.getElementById("tasks-list")
const btnOptions = document.querySelectorAll(".to-do__option")
const eraseButton = document.getElementById("erase-button")



let task
let complete = false
const fragment = document.createDocumentFragment()

const filterCompleted = () => {
    complete = true
    printHTML(tasks.filter(task => task.finish))
}

const filterActive = () => {
    complete = false
    printHTML(tasks.filter(task => !task.finish))
}


// EventListeners

optionContainer.addEventListener("click", e => {
    if (e.target.classList.contains("to-do__option")) {
        btnOptions.forEach(options => options.classList.remove("to-do__option--active"));
        e.target.classList.add("to-do__option--active")

        if (e.target.dataset.id == 1) {
            complete = false
            printHTML(tasks)
        }

        e.target.dataset.id == 2 && filterActive()
        e.target.dataset.id == 3 && filterCompleted()

    }
})

getForm.addEventListener("submit", e => {
    e.preventDefault()
    task = getForm.task.value
    if (task.trim().length > 0) {

        getAddTask(task)

        btnOptions.forEach(option => {
            option.classList.remove("to-do__option--active")
            if (option.dataset.id == "1" && !option.classList.contains("to-do__option--active")) {
                option.classList.add("to-do__option--active")
                complete = false
                printHTML(tasks)
            }
        })
        getForm.reset()
    }
})

tasksContainer.addEventListener("click", e => {
    if (e.target.checked) {
        tasks.forEach(task => {
            if (task.id == e.target.id)
                task.finish = true
        })
    } else {
        tasks.forEach(task => {
            if (task.id == e.target.id)
                task.finish = false
        })
    }
    if (e.target.classList.contains("task__erase")) {
        tasks = tasks.filter(task => task.id !== e.target.dataset.id)
        if (tasks.filter(task => task.id !== e.target.dataset.id).length < 2) eraseButton.classList.remove("erase-button--active")
        filterCompleted()
    }

   
})

// Delete all button
eraseButton.addEventListener("click", () => {
    tasks = tasks.filter(task => !task.finish)
    printHTML(tasks)
    eraseButton.classList.remove("erase-button--active")


    btnOptions.forEach(option => {
        option.classList.remove("to-do__option--active")
        if (option.dataset.id == "1" && !option.classList.contains("to-do__option--active")) {
            option.classList.add("to-do__option--active")
            complete = false
            printHTML(tasks)
        }
    })
})


export { getForm, tasksContainer, optionContainer, btnOptions, eraseButton, fragment, complete }
